# -----------------------------------------------------------------------------
# IAM Lambda Roles and Policies
# -----------------------------------------------------------------------------

resource "aws_iam_role" "default" {
  name = "${var.resource_name_prefix}-${var.role_name}"

  assume_role_policy = templatefile(var.assume_role_policy, var.role_vars)

  tags = var.tags
}

resource "aws_iam_policy" "default" {
  name = "${var.resource_name_prefix}-${var.policy_name}"

  policy = templatefile(var.template, var.role_vars)

  tags = var.tags
}

resource "aws_iam_policy_attachment" "default" {
  name = "${var.resource_name_prefix}-${var.policy_attachment_name}"

  policy_arn = aws_iam_policy.default.arn
  roles      = [aws_iam_role.default.name]
}

